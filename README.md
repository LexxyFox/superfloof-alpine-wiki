# OwO wat's dis?

This is my collection of recipes for Alpine Linux; or alternatively, my custom Alpine Linux distribution whose packages are written in natural language instead of machine readable APKBUILD files (for now...). These recipes are oriented around bare-metal server software, but may also include other domains in the future also.

[View the recipes online at https://codeberg.org/LexxyFox/superfloof-alpine-wiki.wiki.git](https://codeberg.org/LexxyFox/superfloof-alpine-wiki.wiki.git)
